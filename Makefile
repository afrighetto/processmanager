.PHONY: assets
CC = gcc
CFLAGS = -o
LDFLAGS = -pthread
EXECUTABLE = pmanager
SOURCES := $(wildcard $(PWD)/src/*.c) $(wildcard $(PWD)/src/pmanager/*.c)

define SCRIPT_TESTFILE
#!/bin/bash

create_testfile_on_the_fly() {
    list_cmds=("pspawn %s" "ptree" "prmall %s" "pclose %s" "pinfo %s" "plist")
    cmd="#non-deterministic file test created\\n"
    proc_name=$$(date | shasum | head -c 5)
    for (( i=0; i<10; i++ ))
    do
        index=$$((RANDOM % 6))
        if (( i == 0 || i%3 == 0 )); then
            cmd+="pnew $$proc_name"
        else
            cmd+=$$(echo $${list_cmds[$$index]} | sed 's/%s/'"$$proc_name"'/')
        fi
        cmd+="\\n"
    done
    echo -e "$${cmd}quit" > assets/testfile
}

if (( $$((RANDOM % 2)) )); then
    create_testfile_on_the_fly
else
    cp src/testfile assets/
fi

endef

default: help

help:
	@echo "pmanager SO lab project by antoniofrighetto."
	@echo "build it and launch phelp for info and usage."
	@echo "standard rules are applied. enjoy."

compile:
	@echo "compiling...";\
	mkdir -p build && cd build && $(CC) $(CFLAGS) $(EXECUTABLE) $(SOURCES) $(LDFLAGS)

clean:
	@echo "cleaning...";\
	rm -f *.o *~ *.sh *.log;\
	rm -f src/*.o src/*~ src/pmanager/*.o src/pmanager/*~;\
	rm -rf build/;\
	rm -rf assets/;

build: clean compile

export SCRIPT_TESTFILE
assets: build
	@echo "creating script...";\
	mkdir -p assets;\
	echo "$$SCRIPT_TESTFILE" > script.sh;\
	chmod +x script.sh;\
	./script.sh;
	@echo "testfile is now at assets/"

test: assets
	@echo "executing $(EXECUTABLE) in testing mode...";\
	./build/$(EXECUTABLE) assets/testfile;
