### ProcessManager

----

This was the project we were asked to carry out for the Operating System I course at University of Trento. Assignment consisted in creating a basic custom shell with some commands which spawn other processes, list them, clone them, clone process of a process and so on. Get it and enter `phelp` for a list of available commands. Note that we were only allowed to use signals (but not the `sigaction()` interface) and (named) pipes.
