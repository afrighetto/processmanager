//
//  list.h
//  pmanager
//
//  ~antoniofrighetto
//

#ifndef list_h
#define list_h

#include "import.h"

typedef struct node {
    char* name;
    pid_t ppid;
    pid_t pid;
    uint32_t level;
    struct node* next;
} node_t;

void append(node_t** head, const char* name, pid_t ppid, pid_t pid);
node_t* lookup(node_t* node, const char* name);
int delete(node_t** head, const char* name);
void traverse(node_t* node);
node_t* check_valid_node(const char* name);
node_t* get_node_by_pid(pid_t pid);
void increment_level(node_t** node);
void decrement_level(node_t** node);

extern node_t* list;

#endif /* list_h */
