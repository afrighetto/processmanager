//
//  manager.c
//  pmanager
//
//  ~antoniofrighetto
//

#include "manager.h"

static void cleanup(char** args);
static char** split_cmdline(char* line);
static void get_info(const char* name);
static void request_for_term(int sig);
static void child_handler(int sig);
static void request_for_subchild(int sig);
static void spawn_child(void* arg);
static void close_process(const char* name, int flag);
static void spawn_subchild(void* arg);
static void print_tree(node_t* node, pid_t parent_id, int i);

node_t* list = NULL;
volatile sig_atomic_t terminate = 0;
volatile sig_atomic_t proc_status = 0; /* Need to use this var to wait for signal completion before giving back the prompt to user */

void usage(const char* name) {
    printf("Usage: %s [FILE]\n", name);
    printf("Launch a custom shell with the following commands available:\n");
    printf("  plist        show spawned processes\n");
    printf("  pnew NAME    create a new process\n");
    printf("  pinfo NAME   get info about process\n");
    printf("  pclose NAME  close process\n");
    printf("  pspawn NAME  clone existing process\n");
    printf("  prmall NAME  close process with all its clones\n");
    printf("  ptree        show active process hierarchy\n");
    printf("  quit         exit from shell\n");
    printf("Optionally, a file can be specified from where commands should be read.\n");
}

void parse_cmdline(const char* line) {
    proc_status = 0;
    if (!*line)
        return;
    
    pthread_t thread;
    char** args = split_cmdline((char*)line);
    if (!args) {
        __log(LOG_ERROR, "Too many arguments provided or non-alphanumeric chars were included.\n");
        return;
    }
    
    if (strcmp(args[0], "plist") == 0) {
        if (args[1])
            goto invalid;
        if (!list) {
            __log(LOG_WARN, "No process currently opened.\n");
        } else
            traverse(list);
        proc_status = 1;
    } else if (strcmp(args[0], "pnew") == 0) {
        pthread_create(&thread, NULL, (void* (*)(void*))spawn_child, args[1]);
    } else if (strcmp(args[0], "pinfo") == 0) {
        get_info(args[1]);
        proc_status = 1;
    } else if (strcmp(args[0], "pclose") == 0) {
        close_process(args[1], 0);
        proc_status = 1;
    } else if (strcmp(args[0], "pspawn") == 0) {
        pthread_create(&thread, NULL, (void* (*)(void*))spawn_subchild, args[1]);
    } else if (strcmp(args[0], "prmall") == 0) {
        close_process(args[1], 1);
        proc_status = 1;
    } else if (strcmp(args[0], "ptree") == 0) {
        if (args[1])
            goto invalid;
        if (!list) {
            __log(LOG_WARN, "No process spawned yet.\n");
        } else
            print_tree(list, 0, 0);
            proc_status = 1;
    } else {
        goto invalid;
    }
    
    pthread_join(thread, NULL);
    while (!proc_status);
    cleanup(args);
    return;
    
invalid:
    __log(LOG_ERROR, "Invalid command.\n");
    proc_status = 1;
}

void cleanup(char** args) {
    int i;
    for (i = 0; i < 2; i++)
        free(args[i]);
    free(args);
}

char** split_cmdline(char* line) {
    int i = 0, k = 0, j;
    char* tk_line = line;
    char** args = calloc(2, sizeof(char*));
    if (!args)
        __xerror(MEMORY_RESOURCE_SHORTAGE);
    while (*tk_line++) {
        if (i > 1) /* No more than two args allowed */
            goto invalid;
        
        if (*tk_line == ' ' || *tk_line == '\0') {
            size_t len = strlen(line) - strlen(tk_line) - k;
            if ((args[i] = malloc(len + 1)) == NULL)
                __xerror(MEMORY_RESOURCE_SHORTAGE);
            memset(args[i], '\0', len + 1);
            for (j = 0; j < len; j++) {
                args[i][j] = line[j + k];
                if (!isalnum(args[i][j]) && args[i][j] != '_') {
                    goto invalid;
                }
            }
            i++;
            k += (len + 1);
        }
    }
    return args;
    
invalid:
    cleanup(args);
    return NULL;
}

void print_tree(node_t* node, pid_t parent_id, int i) {
    int root_pid = getpid();
    node_t* tmp;
    for (tmp = node; tmp; tmp = tmp->next) {
        if (parent_id != 0 && parent_id != tmp->ppid)
            continue;
        if (parent_id == 0 && tmp->ppid != root_pid) /* Not interested in subchilds if parent_id == 0 (the very first time it is called) */
            continue;
        
        printf("%*s %s\n", i, "┖───", tmp->name);
        if (tmp->level > 0 && tmp->ppid == root_pid) /* If node has ppid == root_pid we left pad by 16 */
            print_tree(node, tmp->pid, i + 16);
        else if (tmp->level > 0 && tmp->ppid != root_pid) /* If node has ppid != root_pid we left pad by 4 */
            print_tree(node, tmp->pid, i + 4);
    }
}

void get_info(const char* name) {
    node_t* node = check_valid_node(name);
    if (!node) {
        return;
    }
    
    int uid = getuid();
    struct passwd* pwd = getpwuid(uid);
    printf("%-10s%-25s%-10s%-10s%-10s\n", "UID", "OWNER", "PID", "PPID", "NAME");
    printf("%-10d%-25s%-10d%-10d%-10s\n", uid, pwd ? pwd->pw_name : "Unknown", node->pid, node->ppid, name);
}

void close_all_subchild(pid_t pid) {
    node_t* q;
    for (q = list; q; q = q->next) {
        if (q->pid != pid && q->ppid != pid)
            continue;
        
        if (q->ppid == pid && q->level > 0)
            close_all_subchild(q->pid);
        
        if (!q->name)
            continue;
        
        int ret = kill(q->pid, SIGTERM);
        if (ret < 0) {
            __xerror(SENDING_SIGNAL_FAILURE);
        }
        __log(LOG_INFO, "Process %s gracefully exited.\n", q->name);
        delete(&list, q->name);
    }
}

void close_process(const char* name, int flag) {
    node_t* node = check_valid_node(name);
    if (!node)
        return;
    
    if (node->level > 0) {
        if (flag) {
            close_all_subchild(node->pid);
        } else {
            __log(LOG_WARN, "%s has currently clones opened, you should quit them before...\n", node->name);
            return;
        }
    } else {
        int ret = kill(node->pid, SIGTERM);
        if (ret < 0) {
            __xerror(SENDING_SIGNAL_FAILURE);
        }
        __log(LOG_INFO, "Process %s gracefully exited.\n", name);
        delete(&list, name);
    }
}

void spawn_subchild(void* arg) {
    node_t* node = check_valid_node((const char*)arg);
    if (!node) {
        proc_status = 1;
        return;
    }
    
    int ret = kill(node->pid, SIGUSR1);
    if (ret < 0) {
        __xerror(SENDING_SIGNAL_FAILURE);
    }
}

void child_handler(int sig) {
    switch (sig) {
        case SIGALRM: {
            char buffer[16];
            int pids[2]; /* pids[0]: parent pid pids[1]: child pid */
            int fd, i = 0;
            fd = open(FIFO_PATH, O_RDONLY);
            read(fd, buffer, sizeof(buffer));
            char* ch = strtok(buffer, "-");
            while (ch) {
                pids[i++] = atoi(ch);
                ch = strtok(NULL, "-");
            }
            char name[64] = {0};
            node_t* node = get_node_by_pid(pids[0]);
            strncpy(name, node->name, sizeof(name) - 16);
            snprintf(name + strlen(name), sizeof(name), "_%d", node->level);
            increment_level(&node);
            append(&list, name, pids[0], pids[1]);
            __log(LOG_INFO, "Process %s cloned.\n", name);
            break;
        }
        case SIGCHLD: {
            int status;
            wait(&status);
            if (!WIFEXITED(status)) {
                __xerror("Something went wrong when quitting process, exiting...\n");
            }
            break;
        }
        default: {
            return;
        }
    }
    proc_status = 1;
}

void alert_parent(__unused int sig) {
    kill(getppid(), SIGALRM);
}

void request_for_term(__unused int sig) {
    terminate = 1;
}

void spawn_child(void* arg) {
    pid_t pid;
    const char* name = (const char*)arg;
    if (!name) {
        __log(LOG_WARN, "Missing name.\n");
        proc_status = 1;
        return;
    }
    node_t* node;
    if ((node = lookup(list, name)) != NULL) {
        __log(LOG_WARN, "Process %s already exists...\n", name);
        proc_status = 1;
        return;
    }
    
    if ((pid = fork()) == 0) {
        signal(SIGTERM, request_for_term);
        signal(SIGUSR1, request_for_subchild);
        while (!terminate);
        exit(0);
    } else if (pid > 0) {
        signal(SIGCHLD, child_handler);
        signal(SIGALRM, child_handler);
        __log(LOG_INFO, "Process %s spawned.\n", name);
        append(&list, name, getpid(), pid);
        proc_status = 1;
    } else {
        __xerror(PROCESS_RESOURCE_SHORTAGE);
    }
}

void wait_for_pid(__unused int sig) {
    int status;
    wait(&status);
    if (!WIFEXITED(status)) {
        __xerror("Something went wrong when quitting process, exiting...\n");
    }
}

void request_for_subchild(__unused int sig) {
    pid_t pid;
    if ((pid = fork()) == 0) {
        signal(SIGTERM, request_for_term);
        signal(SIGUSR1, request_for_subchild);
    } else if (pid > 0) {
        signal(SIGCHLD, wait_for_pid);
        signal(SIGALRM, alert_parent); /* Signal parent to store node in the list */
        char pids[16] = {0};
        snprintf(pids, sizeof(pids), "%d-%d\n", getpid(), pid);
        int fifo_fd;
        /* We open a FIFO so that parent can read pid values otherwise we should maintain the same list for childs */
        if ((fifo_fd = open(FIFO_PATH, O_RDWR|O_TRUNC)) < 0)
            __xerror("Could not open "FIFO_PATH"\n");
        write(fifo_fd, pids, sizeof(pids));
        alert_parent(0); /* Alert parent that a process was cloned */
    } else {
        __xerror(PROCESS_RESOURCE_SHORTAGE);
    }
}
