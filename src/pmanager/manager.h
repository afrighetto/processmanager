//
//  manager.h
//  pmanager
//
//  ~antoniofrighetto
//

#ifndef manager_h
#define manager_h

#include "import.h"

void usage(const char* name);
void parse_cmdline(const char* line);

#endif /* manager_h */
