//
//  list.c
//  pmanager
//
//  ~antoniofrighetto
//

#include "list.h"

void append(node_t** head, const char* name, pid_t ppid, pid_t pid) {
    node_t* node = malloc(sizeof(node_t));
    if (!node)
        __xerror(MEMORY_RESOURCE_SHORTAGE);
    if ((node->name = malloc(strlen(name) + 1)) == NULL)
        __xerror(MEMORY_RESOURCE_SHORTAGE);
    strcpy(node->name, name);
    node->ppid = ppid;
    node->pid = pid;
    node->next = *head;
    node->level = 0;
    *head = node;
}

node_t* lookup(node_t* node, const char* name) {
    while (node) {
        if (strcmp(node->name, name) == 0)
            return node;
        node = node->next;
    }
    return NULL;
}

int delete(node_t** head, const char* name) {
    if (!*head || !name)
        return -1;
    node_t** curr = head;
    while (*curr) {
        if (strcmp((*curr)->name, name) == 0) {
            node_t* parent_node = get_node_by_pid((*curr)->ppid);
            decrement_level(&parent_node);
            node_t* next = (*curr)->next;
            free((*curr)->name);
            free(*curr);
            (*curr)->name = NULL;
            *curr = next;
            return 0;
        }
        curr = &(*curr)->next;
    }
    return -1;
}

void traverse(node_t* node) {
    static int i = 0;
    if (!node) {
        i = 0;
        return;
    }
    
    traverse(node->next);
    printf("%d - %s\n", i, node->name);
    i++;
}

void increment_level(node_t** node) {
    (*node)->level++;
}

void decrement_level(node_t** node) {
    if (*node && (*node)->level) {
        (*node)->level--;
    }
}

node_t* check_valid_node(const char* name) {
    if (!name) {
        __log(LOG_WARN, "Missing process name.\n");
        return NULL;
    }
    
    node_t* node;
    if ((node = lookup(list, name)) == NULL) {
        __log(LOG_ERROR, "Invalid process name.\n");
        return NULL;
    }
    return node;
}

node_t* get_node_by_pid(pid_t pid) {
    node_t* q = list;
    while (q) {
        if (q->pid == pid) {
            return q;
        }
        q = q->next;
    }
    return NULL;
}
