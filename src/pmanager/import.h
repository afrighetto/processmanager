//
//  import.h
//  pmanager
//
//  ~antoniofrighetto
//

#ifndef import_h
#define import_h

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <pwd.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#include "list.h"
#include "manager.h"

#define MAX_CMD_LENGTH  64
#define FIFO_PATH       "/tmp/pmanagerfifo"
#define LOG_PATH        "output.log"
#define LOG_INFO        1 << 0
#define LOG_WARN        1 << 1
#define LOG_ERROR       1 << 2
#define COLOR_RED       "\033[31;1m"
#define COLOR_GREEN     "\033[32;1m"
#define COLOR_YELLOW    "\033[33;1m"
#define COLOR_NONE      "\033[0m"
#define MEMORY_RESOURCE_SHORTAGE   "Out of memory, exiting...\n"
#define PROCESS_RESOURCE_SHORTAGE  "Could not spawn further processes, exiting...\n"
#define SENDING_SIGNAL_FAILURE     "Could not send signal to process, exiting...\n"
#ifndef __unused
    #define __unused __attribute__((unused))
#endif

#define __xerror(msg) do {  \
    fprintf(stderr, "[%s@%d] "msg, __func__, __LINE__);  \
    exit(1);  \
} while(0);

#define __log(flag, msg, ...) do {  \
    if (flag & LOG_INFO)  \
        fprintf(stdout, "[%s+%s] "msg, COLOR_GREEN, COLOR_NONE, ##__VA_ARGS__);  \
    else if (flag & LOG_WARN)  \
        fprintf(stdout, "[%s!%s] "msg, COLOR_YELLOW, COLOR_NONE, ##__VA_ARGS__);  \
    else  \
        fprintf(stderr, "[%s-%s] "msg, COLOR_RED, COLOR_NONE, ##__VA_ARGS__);  \
} while(0);

#endif /* import_h */
