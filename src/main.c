//
//  main.c
//  pmanager
//
//  ~antoniofrighetto
//

#include <stdio.h>

#include "pmanager/import.h"

int check_line(const char* arg, char* line) {
    if (strcmp(line, "phelp") == 0) {
        usage(arg);
    } else if (strcmp(line, "quit") == 0) {
        return 0;
    } else {
        parse_cmdline(line);
    }
    return 1;
}

int main(int argc, const char** argv) {
    char line[MAX_CMD_LENGTH] = {0};
    size_t pos;
    int ch, ret;
    
    if (argc > 2)
        __xerror("Invalid arguments.\n");
    
    while ((ret = mknod(FIFO_PATH, 0666|S_IFIFO, 0)) != 0) {
        if (errno == EEXIST) {
            unlink(FIFO_PATH);
        } else
            __xerror("Could not create a named pipe, exiting...\n");
    }
    
    if (argc == 2) {
        FILE* fp;
        if ((fp = fopen(argv[1], "r")) == NULL) {
            fclose(fp);
            __xerror("Could not open file, exiting...\n");
        }
        
        int fd;
        if ((fd = open(LOG_PATH, O_RDWR|O_CREAT|O_APPEND, 0666)) < 0) {
            __xerror("Could not open file for logging, exiting...\n");
        }
        char buffer[64] = {0};
        time_t now = time(NULL);
        snprintf(buffer, sizeof(buffer), "Output of %s", asctime(localtime(&now)));
        write(fd, buffer, sizeof(buffer));
        
        int current_stdout = dup(STDOUT_FILENO);
        __log(LOG_INFO, "Processing %s...\n", argv[1]);
        dup2(fd, STDOUT_FILENO);
        dup2(fd, STDERR_FILENO);
        
        while (fgets(line, sizeof(line), fp) != NULL) {
            if (line[strlen(line) - 1] == '\n')
                line[strlen(line) - 1] = '\0';
            if (*line == '#' || *line == '\0')
                continue;
            if (check_line(argv[0], line) == 0)
                break;
            usleep(100000);
        }
        
        write(fd, "\n", sizeof("\n"));
       	fflush(stdout);
        fclose(fp);
        close(fd);
        dup2(current_stdout, STDOUT_FILENO);
        __log(LOG_INFO, "Redirected output to %s\n", LOG_PATH);
    } else {
        while (1) {
            pos = 0;
            memset(line, '\0', sizeof(line));
            printf("%s>%s ", COLOR_YELLOW, COLOR_NONE);
            while ((ch = fgetc(stdin)) != EOF && ch != '\n') {
                line[pos++] = ch;
                if (pos == MAX_CMD_LENGTH - 1) {
                    line[MAX_CMD_LENGTH - 1] = '\0';
                    break;
                }
            }
            
            if (check_line(argv[0], line) == 0)
                break;
        }
    }
    
    unlink(FIFO_PATH);
    return 0;
}
